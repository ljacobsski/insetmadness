//
//  ViewController.swift
//  insetMadness
//
//  Created by Lincoln Jacobs on 6/23/16.
//  Copyright © 2016 Lincoln Jacobs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var includeImageSwitch: UISwitch!

    @IBOutlet weak var imgInsetLeft: UITextField!
    @IBOutlet weak var imgInsetRight: UITextField!
    @IBOutlet weak var txtInsetLeft: UITextField!
    @IBOutlet weak var txtInsetRight: UITextField!
    @IBOutlet weak var cntntInsetLeft: UITextField!
    @IBOutlet weak var cntntInsetRight: UITextField!
    
    var imageNormal: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myButton.layer.cornerRadius = 10;
        myButton.layer.borderWidth = 1;
        myButton.layer.borderColor = UIColor.blueColor().CGColor
        
        imageNormal = UIImage(named: "btnIcon")
        setButtonImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func tapMyButton(sender: UIButton) {
        // set insets
        let contentInsetTop:CGFloat = 8//myButton.contentEdgeInsets.top
        let contentInsetBottom:CGFloat = 8//myButton.contentEdgeInsets.bottom
        let imgInsetTop = myButton.imageEdgeInsets.top
        let imgInsetBottom = myButton.imageEdgeInsets.bottom
        let ttlInsetTop = myButton.titleEdgeInsets.top
        let ttlInsetBottom = myButton.titleEdgeInsets.bottom
        
        myButton.imageEdgeInsets = UIEdgeInsetsMake(imgInsetTop, CGFloat((imgInsetLeft.text! as NSString).floatValue), imgInsetBottom, CGFloat((imgInsetRight.text! as NSString).floatValue))
        myButton.titleEdgeInsets = UIEdgeInsetsMake(ttlInsetTop, CGFloat((txtInsetLeft.text! as NSString).floatValue), ttlInsetBottom, CGFloat((txtInsetRight.text! as NSString).floatValue))
        myButton.contentEdgeInsets = UIEdgeInsetsMake(contentInsetTop, CGFloat((cntntInsetLeft.text! as NSString).floatValue), contentInsetBottom, CGFloat((cntntInsetRight.text! as NSString).floatValue))
        
        // viewNeedsLayout
        myButton.layer.setNeedsLayout()
    }
    
    func setButtonImage() {
        if includeImageSwitch.on {
            myButton.setImage(imageNormal, forState: UIControlState.Normal)
            myButton.setImage(imageNormal, forState: UIControlState.Highlighted)

        } else {
            myButton.setImage(UIImage(), forState: UIControlState.Normal)
            myButton.setImage(UIImage(), forState: UIControlState.Highlighted)
        }
    }
}

